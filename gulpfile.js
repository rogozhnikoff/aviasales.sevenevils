var postcss = require('gulp-postcss');
var gulp = require('gulp');
var autoprefixer = require('autoprefixer');

gulp.task('css', function () {
	var processors = [
		autoprefixer({browsers: ['last 20 version']})
	];
	return gulp.src('./style/*.css')
			.pipe(postcss(processors))
			.pipe(gulp.dest('./style-build'));
});

gulp.task('default', ['css']);